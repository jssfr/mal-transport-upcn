/**********************************************************************
LICENSE

File name: MessageTransceiver.java
This file is part of: mal-transport-upcn

Copyright (c) 2019 Jonas Schäfer, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************/
package de.dtnsat.mal.upcn.app;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.io.EOFException;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Future;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Logger;
import java.util.logging.Level;
import de.dtnsat.mal.upcn.msg.ApplicationMessage;
import de.dtnsat.mal.upcn.msg.SendBundleMessage;
import de.dtnsat.mal.upcn.msg.MessageParser;
import de.dtnsat.mal.upcn.msg.MessageException;

public class MessageTransceiver implements Runnable {
    private final Logger logger = Logger.getLogger("UPCN-MessageTransceiver");
    private final InputStream is;
    private final OutputStream os;
    private final Object lock = new Object();
    private final List<Matcher> matchers = new ArrayList<>();
    private final MessageSink messageSink;

    private class Matcher {
        private List<ApplicationMessage.Type> selectedTypes = null;
        private final CompletableFuture<ApplicationMessage> future =
            new CompletableFuture<>();

        public Matcher(ApplicationMessage.Type... types)
        {
            this.selectedTypes = Arrays.asList(types);
        }

        public boolean matches(ApplicationMessage.Type type) {
            return selectedTypes.contains(type);
        }

        public CompletableFuture<ApplicationMessage> getFuture() {
            return this.future;
        }
    }

    public MessageTransceiver(InputStream is, OutputStream os,
                              MessageSink handler)
    {
        this.is = is;
        this.os = os;
        this.messageSink = handler;
    }

    public Future<ApplicationMessage> sendWithAck(ApplicationMessage msg)
    {
        Matcher matcher = new Matcher(
            ApplicationMessage.Type.ACK,
            ApplicationMessage.Type.NACK
            );
        logger.log(Level.FINEST, "sending with ack: {0}", new Object[]{msg});
        try {
            synchronized (lock) {
                msg.serialise(os);
                os.flush();
                matchers.add(matcher);
            }
            return matcher.getFuture();
        } catch (IOException e) {
            CompletableFuture<ApplicationMessage> result =
                new CompletableFuture<>();
            result.completeExceptionally(e);
            return result;
        }
    }

    public Future<ApplicationMessage> sendBundle(
        SendBundleMessage msg)  throws IOException
    {
        Matcher matcher = new Matcher(
            ApplicationMessage.Type.NACK,
            ApplicationMessage.Type.SENDCONFIRM
            );
        logger.log(Level.FINEST, "sending as bundle: {0}", new Object[]{msg});
        try {
            synchronized (lock) {
                msg.serialise(os);
                os.flush();
                matchers.add(matcher);
            }
            return matcher.getFuture();
        } catch (IOException e) {
            CompletableFuture<ApplicationMessage> result =
                new CompletableFuture<>();
            result.completeExceptionally(e);
            return result;
        }
    }

    public void sendWithoutAck(
        ApplicationMessage msg)
    {
        logger.log(Level.FINEST, "sending without ack: {0}", new Object[]{msg});
        try {
            synchronized (lock) {
                msg.serialise(os);
                os.flush();
            }
        } catch (IOException e) {
            // errors are silenced for things sent without ack
        }
    }

    private void processSingle() throws IOException, MessageException {
        logger.log(Level.FINEST, "parsing message");
        ApplicationMessage msg = MessageParser.parse(is);
        logger.log(Level.FINER, "received message: {0}", new Object[]{msg});
        if (msg.getType() == ApplicationMessage.Type.RECVBUNDLE) {
            messageSink.handleMessage(msg);
            return;
        }

        Matcher matched = null;

        synchronized (lock) {
            if (!matchers.isEmpty()) {
                Matcher nextMatcher = matchers.get(0);
                if (nextMatcher.matches(msg.getType())) {
                    matchers.remove(0);
                    matched = nextMatcher;
                }
            }
        }

        if (null == matched) {
            messageSink.handleMessage(msg);
        } else {
            matched.getFuture().complete(msg);
        }

        logger.log(Level.FINEST, "message handled: {0}", new Object[]{msg});
    }

    @Override
    public void run() {
        logger.log(Level.FINEST, "thread started up");
        while (true) {
            try {
                processSingle();
            } catch (EOFException e) {
                logger.log(Level.INFO, "received EOF");
                messageSink.handleTransportFailure(e);
                break;
            } catch (Exception e) {
                // do something sensible!
                logger.log(Level.SEVERE, "MessageTransceiver crashed!", e);
                messageSink.handleTransportFailure(e);
                break;
            }
        }
    }
}
