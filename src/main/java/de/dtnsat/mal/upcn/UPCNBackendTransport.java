/**********************************************************************
LICENSE

File name: UPCNBackendTransport.java
This file is part of: mal-transport-upcn

Copyright (c) 2019 Jonas Schäfer, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.dtnsat.mal.upcn;

import static esa.mo.mal.transport.gen.GENTransport.LOGGER;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.IOException;
import org.ccsds.moims.mo.mal.MALException;
import org.ccsds.moims.mo.testbed.util.spp.SPPSocket;
import org.ccsds.moims.mo.testbed.util.spp.SPPSocketFactory;
import org.ccsds.moims.mo.testbed.util.sppimpl.tcp.ClientTCPSPPSocket;
import org.ccsds.moims.mo.testbed.util.sppimpl.util.SPPHelper;

/**
 *
 * @author masterthesis
 */
public class UPCNBackendTransport {
    private final ClientTCPSPPSocket upcn_side;
    private final SPPSocket hw_side;

    private final UPCNTransportPipe hw_to_upcn;
    private final UPCNTransportPipe upcn_to_hw;

    private final Process upcn;

    public UPCNBackendTransport(Map properties) throws MALException
    {
        // first, start the uPCN process
        try {
            final String upcn_executable =
                (String)properties.get("de.dtnsat.mal.upcn.exec");
            if ((null != upcn_executable) && (0 != upcn_executable.length())) {
                upcn = new ProcessBuilder(upcn_executable).inheritIO().start();
                // make sure that uPCN gets killed properly
                Runtime.getRuntime().addShutdownHook(new Thread(upcn::destroy));
            } else {
                LOGGER.log(Level.WARNING, "upcn executable not configured, will not start and manage upcn process");
                upcn = null;
            }
        } catch (IOException e) {
            throw new MALException("failed to start uPCN: "+e);
        }

        // this is a crude hack
        LOGGER.log(Level.INFO, "starting up sockets for uPCN backend pipe");
        try {
            hw_side = SPPSocketFactory.newInstance().createSocket(properties);
        } catch (Exception e) {
            throw new MALException("Failed to connect to hardware SPP layer: "+e);
        }
        LOGGER.log(Level.INFO, "hw side socket started");
        upcn_side = new ClientTCPSPPSocket();

        Map<String, String> socketCfg = new HashMap<>(properties);
        socketCfg.put(
            ClientTCPSPPSocket.HOSTNAME,
            (String)properties.get("de.dtnsat.mal.upcn.spp.host")
        );
        socketCfg.put(
            ClientTCPSPPSocket.PORT,
            (String)properties.get("de.dtnsat.mal.upcn.spp.port")
        );
        try {
            upcn_side.init(socketCfg);
        } catch (Exception ex) {
            throw new MALException("Failed to connect to uPCN: "+ex);
        }
        LOGGER.log(Level.INFO, "upcn side socket started");

        hw_to_upcn = new UPCNTransportPipe(hw_side, upcn_side);
        upcn_to_hw = new UPCNTransportPipe(upcn_side, hw_side);
    }

    public void start() {
        hw_to_upcn.start();
        upcn_to_hw.start();
    }
}
