/**********************************************************************
LICENSE

File name: MessageUtil.java
This file is part of: mal-transport-upcn

Copyright (c) 2019 Jonas Schäfer, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************/
package de.dtnsat.mal.upcn.msg;

import java.io.IOException;
import java.io.EOFException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;
import java.util.logging.Level;

public class MessageUtil {
    private static final Logger logger = Logger.getLogger("UPCN-MessageUtil");

    private MessageUtil() {

    }

    public static byte[] encodeEID(String eid) {
        return eid.getBytes(StandardCharsets.UTF_8);
    }

    public static void writeEID(OutputStream os, byte[] eid) throws IOException {
        os.write(ByteBuffer.allocate(2).order(ByteOrder.BIG_ENDIAN).putShort((short)eid.length).array());
        os.write(eid);
    }

    public static byte[] readEID(InputStream is) throws IOException {
        ByteBuffer buf = ByteBuffer.allocate(2).order(ByteOrder.BIG_ENDIAN);
        logger.log(Level.FINEST, "readEID(...): reading length");
        MessageUtil.readExactly(is, buf.array());
        int len = buf.get() << 8;
        len |= buf.get();
        logger.log(Level.FINEST, "readEID(...): got length: {0}", new Object[]{
            new Integer(len)
        });
        logger.log(Level.FINEST, "readEID(...): reading EID");
        byte[] eid = new byte[len];
        readExactly(is, eid);
        logger.log(Level.FINEST, "readEID(...): got EID, returning");
        return eid;
    }

    public static String decodeEID(byte[] eid) {
        return new String(eid, StandardCharsets.UTF_8);
    }

    public static byte[] readPayload(InputStream is) throws IOException {
        ByteBuffer buf = ByteBuffer.allocate(8).order(ByteOrder.BIG_ENDIAN);
        MessageUtil.readExactly(is, buf.array());
        long len = buf.getLong();
        // this is slightly evil...
        byte[] payload = new byte[(int)len];
        readExactly(is, payload);
        return payload;
    }

    public static long readBundleID(InputStream is) throws IOException {
        ByteBuffer buf = ByteBuffer.allocate(8).order(ByteOrder.BIG_ENDIAN);
        MessageUtil.readExactly(is, buf.array());
        return buf.getLong();
    }

    public static void writeBundleID(OutputStream os, long bundleID) throws IOException {
        os.write(ByteBuffer.allocate(8).order(ByteOrder.BIG_ENDIAN).putLong(bundleID).array());
    }

    public static void writePayload(OutputStream os, byte[] payload) throws IOException {
        os.write(ByteBuffer.allocate(8).order(ByteOrder.BIG_ENDIAN).putLong(payload.length).array());
        os.write(payload);
    }

    public static void readExactly(InputStream is, byte[] buf) throws IOException {
        logger.log(Level.FINEST, "readExactly(..., {0})", new Object[]{new Integer(buf.length)});
        int offset = 0;
        int len = buf.length;
        while (len > 0) {
            int read = is.read(buf, offset, len);
            if (read < 0) {
                throw new EOFException();
            }
            len -= read;
            offset += read;
        }
    }
}
