/**********************************************************************
LICENSE

File name: UPCNApplicationSocket.java
This file is part of: mal-transport-upcn

Copyright (c) 2019 Jonas Schäfer, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************/
package de.dtnsat.mal.upcn.app;

import java.io.IOException;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.Future;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.net.Socket;
import java.util.logging.Logger;
import java.util.logging.Level;

import de.dtnsat.mal.upcn.msg.ApplicationMessage;
import de.dtnsat.mal.upcn.msg.RegisterMessage;
import de.dtnsat.mal.upcn.msg.WelcomeMessage;
import de.dtnsat.mal.upcn.msg.MessageParser;
import de.dtnsat.mal.upcn.msg.MessageException;
import de.dtnsat.mal.upcn.msg.RecvBundleMessage;
import de.dtnsat.mal.upcn.msg.SendBundleMessage;

public class UPCNApplicationSocket implements MessageSink {

    public interface PayloadReceiver {
        public void handlePayload(final String sourceURI,
                                  final String destURI,
                                  final byte[] payload);
        public void handleTransportFailure(Throwable e);
    }

    private final Logger logger = Logger.getLogger("UPCN-ApplicationSocket");
    private Socket sock = null;
    private MessageTransceiver trx = null;
    private final MessageProcessor processor;
    private String boundURI = null;
    private final String appPrefix;
    private String prefixURI = null;
    private CompletableFuture<WelcomeMessage> welcomeFut = new CompletableFuture<>();
    private volatile PayloadReceiver receiver = null;
    private Thread worker = null;
    private Thread forwarder = null;

    public UPCNApplicationSocket(
        String appPrefix, String host, int port,
        PayloadReceiver receiver) throws IOException
    {
        this.appPrefix = appPrefix;
        this.receiver = receiver;
        this.processor = new MessageProcessor(this);
        setupSocket(new Socket(host, port));
    }

    public UPCNApplicationSocket(
        String appPrefix, Socket sock,
        PayloadReceiver receiver) throws IOException
    {
        this.appPrefix = appPrefix;
        this.receiver = receiver;
        this.processor = new MessageProcessor(this);
        setupSocket(sock);
    }

    private void setupSocket(Socket sock) throws IOException {
        this.sock = sock;
        this.trx = new MessageTransceiver(
            new BufferedInputStream(this.sock.getInputStream()),
            new BufferedOutputStream(this.sock.getOutputStream()),
            this.processor
        );
    }

    private void start() {
        worker = new Thread(this.trx, "upcn-client-socket-trx:"+sock.getLocalPort());
        worker.start();
        forwarder = new Thread(this.processor, "upcn-client-socket-fwd:"+sock.getLocalPort());
        forwarder.start();
    }

    private void setThreadNames(String uri) {
        final String prefix = "upcn-";
        final String infix = uri;
        final String suffix = ":" + sock.getLocalPort();
        worker.setName(prefix + "trx-" + infix + suffix);
        forwarder.setName(prefix + "fwd-" + infix + suffix);
    }

    public void setReceiver(PayloadReceiver receiver) {
        this.receiver = receiver;
    }

    public void handleMessage(ApplicationMessage msg) {
        logger.log(Level.FINEST, "received message: {0}",
                    new Object[]{msg});
        if (msg instanceof WelcomeMessage && !welcomeFut.isDone()) {
            welcomeFut.complete((WelcomeMessage)msg);
        } else if (msg instanceof RecvBundleMessage) {
            RecvBundleMessage recvmsg = (RecvBundleMessage)msg;
            if (null == receiver) {
                logger.log(Level.WARNING,
                           "received message, but no receiver configured!");
            } else {
                receiver.handlePayload(recvmsg.getPeer(),
                                       this.boundURI,
                                       recvmsg.getPayload());
            }
        } else {
            logger.log(Level.WARNING,
                       "unhandled unsolicited message from peer: "+msg);
        }
    }

    public void handleTransportFailure(Throwable ex) {
        logger.log(Level.SEVERE, "Transport for "+this.boundURI+":"+sock.getLocalPort()+" failed!");
        welcomeFut.completeExceptionally(ex);
        receiver.handleTransportFailure(ex);
    }

    public void startConnection() throws HandshakeException, IOException {
        this.start();

        WelcomeMessage welcome = null;

        logger.log(
            Level.FINEST,
            "starting connection, waiting for welcome..."
        );

        try {
            welcome = welcomeFut.get();
        } catch (Throwable e) {
            logger.log(
                Level.WARNING,
                "failed to receive welcome message!"
            );
            throw new HandshakeException("handshake failed: "+e);
        }

        prefixURI = welcome.getEID();
        logger.log(Level.FINE,
                   "received welcome message. prefix is {0}",
                   new Object[]{prefixURI});
    }

    public String getBoundURI() {
        return this.boundURI;
    }

    public String bind(String serviceName) throws InterruptedException, ExecutionException {
        String suffix = serviceName;
        if (!this.appPrefix.isEmpty()) {
            suffix = this.appPrefix + "/" + suffix;
        }

        logger.log(Level.FINE, "attempting to bind to {0}", suffix);
        RegisterMessage register = new RegisterMessage(suffix);
        boolean accepted =
            this.trx.sendWithAck(register).get().getType() ==
            ApplicationMessage.Type.ACK;

        if (accepted) {
            this.boundURI = this.prefixURI + "/" + suffix;
            logger.log(Level.INFO, "bound to URI: {0}", boundURI);
            setThreadNames(suffix);
            return this.boundURI;
        } else {
            logger.log(Level.WARNING, "failed to bind (got NACK)");
            return null;
        }
    }

    public Future<ApplicationMessage> send(
        String destURI, byte[] payload) throws IOException
    {
        SendBundleMessage msg = new SendBundleMessage(destURI, payload);
        return this.trx.sendBundle(msg);
    }

    public void close() {
        // TODO: implement close() >:)
    }
}
