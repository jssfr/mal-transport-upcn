/**********************************************************************
LICENSE

File name: ApplicationMessage.java
This file is part of: mal-transport-upcn

Copyright (c) 2019 Jonas Schäfer, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************/
package de.dtnsat.mal.upcn.msg;

import java.io.IOException;
import java.io.OutputStream;

public interface ApplicationMessage {
    public static enum Type {
        ACK (0x00),
        NACK (0x01),
        REGISTER (0x02),
        SENDBUNDLE (0x03),
        RECVBUNDLE (0x04),
        SENDCONFIRM (0x05),
        CANCELBUNDLE (0x06),
        WELCOME (0x07),
        PING (0x08);

        public final int value;

        Type(int value)
        {
            this.value = value;
        }

        public static Type parse(byte v)
        {
            if ((v & 0xf0) != 0x10) {
                return null;
            }

            v = (byte)(v & 0x0f);
            for (Type t: Type.values()) {
                if (t.value == v) {
                    return t;
                }
            }

            return null;
        }
    }

    default byte getVersionType()
    {
        return (byte)(0x10 | getType().value);
    }

    public void serialise(OutputStream os) throws IOException;
    public Type getType();
}
