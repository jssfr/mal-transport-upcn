/**********************************************************************
LICENSE

File name: UPCNTransport.java
This file is part of: mal-transport-upcn

Copyright (c) 2019 Jonas Schäfer, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.dtnsat.mal.upcn;

import esa.mo.mal.transport.gen.GENMessage;
import esa.mo.mal.transport.gen.GENMessageHeader;
import esa.mo.mal.transport.gen.GENTransport;
import esa.mo.mal.transport.gen.sending.GENMessageSender;
import esa.mo.mal.transport.gen.sending.GENOutgoingMessageHolder;
import esa.mo.mal.transport.gen.util.GENMessagePoller;
import esa.mo.mal.transport.spp.SPPMessageDecoderFactory;
import esa.mo.mal.transport.spp.SPPMessageReceiver;
import esa.mo.mal.transport.spp.SPPMessageSender;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Handler;
import java.util.logging.Logger;
import org.ccsds.moims.mo.mal.MALHelper;
import org.ccsds.moims.mo.mal.MALException;
import org.ccsds.moims.mo.mal.MALStandardError;
import org.ccsds.moims.mo.mal.broker.MALBrokerBinding;
import org.ccsds.moims.mo.mal.encoding.MALElementOutputStream;
import org.ccsds.moims.mo.mal.encoding.MALElementStreamFactory;
import org.ccsds.moims.mo.mal.structures.Blob;
import org.ccsds.moims.mo.mal.structures.InteractionType;
import org.ccsds.moims.mo.mal.structures.QoSLevel;
import org.ccsds.moims.mo.mal.structures.UInteger;
import org.ccsds.moims.mo.mal.structures.URI;
import org.ccsds.moims.mo.mal.transport.MALEndpoint;
import org.ccsds.moims.mo.mal.transport.MALTransportFactory;
import org.ccsds.moims.mo.mal.transport.MALTransmitErrorException;
import org.ccsds.moims.mo.mal.transport.MALTransport;
import org.ccsds.moims.mo.testbed.util.spp.SpacePacket;
import org.ccsds.moims.mo.testbed.util.sppimpl.tcp.ClientTCPSPPSocket;
import de.dtnsat.mal.upcn.app.UPCNApplicationSocket;
import de.dtnsat.mal.upcn.app.HandshakeException;

/**
 *
 * @author masterthesis
 */
public class UPCNTransport extends GENTransport<byte[], byte[]> {

    private final Logger logger = Logger.getLogger("upcn-transport");

    private final UPCNBackendTransport backend;
    private final String upcnAppHost;
    private final int upcnAppPort;
    private final String upcnAppPrefix;

    private int localNameCtr = 0;
    private Object localNameCtrLock;

    public UPCNTransport(String protocol, MALTransportFactory factory, Map properties) throws MALException {
        super(protocol, "://", '/', '@', false, false, factory, properties);

        localNameCtrLock = new Object();

        logger.setLevel(Level.FINEST);
        for (Handler h: logger.getLogger("").getHandlers()) {
            logger.log(Level.INFO, "found handler; log level: %s", h.getLevel());
            h.setLevel(Level.FINEST);
        }
        logger.log(Level.INFO, "test info");
        logger.log(Level.FINEST, "test finest");

        upcnAppHost = (String)properties.get("de.dtnsat.mal.upcn.app.host");
        upcnAppPort = Integer.parseInt(
            (String)properties.get("de.dtnsat.mal.upcn.app.port")
        );
        upcnAppPrefix = (String)properties.get("de.dtnsat.mal.upcn.app.prefix");

        backend = new UPCNBackendTransport(properties);
        backend.start();
    }

    @Override
    public void init() {
        // establish connection to uPCN here
    }

    @Override
    public GENMessage createMessage(byte[] packet) throws MALException {
        /* GENMessage hdr = new GENMessageHeader();
        hdr.setURIFrom() */
        GENMessage msg = new GENMessage(
            wrapBodyParts,
            true,
            new GENMessageHeader(),
            qosProperties,
            packet,
            getStreamFactory()
        );
        return msg;
    }

    @Override
    protected GENOutgoingMessageHolder<byte[]> internalEncodeMessage(
        final String destinationRootURI,
        final String destinationURI,
        final Object multiSendHandle,
        final boolean lastForHandle,
        final String targetURI,
        final GENMessage msg) throws MALTransmitErrorException
    {
        try {
            final ByteArrayOutputStream llOutputStream = new ByteArrayOutputStream();
            final MALElementOutputStream elementStream = getStreamFactory().createOutputStream(llOutputStream);

            msg.encodeMessage(getStreamFactory(), elementStream, llOutputStream, true);
            byte[] data = llOutputStream.toByteArray();
            return new GENOutgoingMessageHolder<byte[]>(
                10,
                destinationRootURI,
                destinationURI,
                multiSendHandle,
                lastForHandle,
                msg,
                data
            );
        } catch (MALException ex) {
            logger.log(Level.SEVERE, "Could not encode message!", ex);
            throw new MALTransmitErrorException(
                msg.getHeader(),
                new MALStandardError(MALHelper.BAD_ENCODING_ERROR_NUMBER, null),
                null
            );
        }
    }

    @Override
    protected String createTransportAddress() {
        return "unset";
    }

    @Override
    protected GENMessageSender createMessageSender(GENMessage msg, String remoteRootURI) {
        logger.log(Level.INFO, "createMessageSender called");
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected UPCNEndpoint internalCreateEndpoint(
        final String localName,
        final String routingName,
        final Map qosProperties
        ) throws MALException
    {
        logger.log(Level.INFO, "internalCreateEndpoint({0}, {1})", new Object[]{
            localName,
            routingName
        });

        UPCNApplicationSocket socket = null;
        try {
            socket = new UPCNApplicationSocket(
                upcnAppPrefix,
                upcnAppHost,
                upcnAppPort,
                null
            );
        } catch (IOException e) {
            throw new MALException(
                "failed to connect to uPCN on the application side: "+e
            );
        }

        try {
            socket.startConnection();
            if (socket.bind(routingName) == null) {
                socket.close();
                throw new MALException("failed to bind to URI");
            }
        } catch (IOException | HandshakeException | InterruptedException | ExecutionException e) {
            socket.close();
            throw new MALException("failed to bind to URI: "+e);
        }

        return new UPCNEndpoint(
            this,
            localName,
            routingName,
            socket,
            true
        );
    }

    @Override
    protected String getLocalName(String localName, final java.util.Map properties)
    {
        /* we use predictable sequential numbers here to be able to create
           upcn contacts more easily */
        if ((null == localName) || (0 == localName.length())) {
            // generate unique local name
            int numberToUse;
            synchronized (localNameCtrLock) {
                numberToUse = localNameCtr;
                localNameCtr = localNameCtr + 1;
            }
            return Integer.toString(numberToUse);
        }

        return localName;
    }

    @Override
    public boolean isSupportedInteractionType(InteractionType type) {
        return type != InteractionType.PUBSUB;
    }

    @Override
    public boolean isSupportedQoSLevel(QoSLevel qos) {
        return true;
    }

    @Override
    public MALEndpoint getEndpoint(URI uri) throws IllegalArgumentException {
        logger.log(Level.INFO, "getEndpoint called");
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public MALBrokerBinding createBroker(String string, Blob blob, QoSLevel[] qsls, UInteger ui, Map map) throws IllegalArgumentException, MALException {
        return null; // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public MALBrokerBinding createBroker(MALEndpoint male, Blob blob, QoSLevel[] qsls, UInteger ui, Map map) throws IllegalArgumentException, MALException {
        return null; // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
