/**********************************************************************
LICENSE

File name: UPCNEndpoint.java
This file is part of: mal-transport-upcn

Copyright (c) 2019 Jonas Schäfer, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************/
package de.dtnsat.mal.upcn;

import esa.mo.mal.transport.gen.GENEndpoint;
import esa.mo.mal.transport.gen.GENMessage;
import esa.mo.mal.transport.gen.sending.GENOutgoingMessageHolder;
import java.util.logging.Logger;
import java.util.logging.Level;
import org.ccsds.moims.mo.mal.MALException;
import org.ccsds.moims.mo.mal.MALHelper;
import org.ccsds.moims.mo.mal.MALStandardError;
import org.ccsds.moims.mo.mal.transport.MALTransmitErrorException;

import de.dtnsat.mal.upcn.app.UPCNApplicationSocket;


public class UPCNEndpoint extends GENEndpoint implements UPCNApplicationSocket.PayloadReceiver
{
    private final UPCNApplicationSocket socket;
    private final Logger logger = Logger.getLogger("UPCN-Endpoint");

    public UPCNEndpoint(final UPCNTransport transport,
                        final String localName,
                        final String routingName,
                        final UPCNApplicationSocket socket,
                        final boolean wrapBodyParts)
    {
        super(transport, localName, routingName, socket.getBoundURI(), wrapBodyParts);
        socket.setReceiver(this);
        logger.setLevel(Level.FINEST);
        this.socket = socket;
    }

    protected void internalSendMessage(
        final Object multiSendHandle,
        final boolean lastForHandle,
        final GENMessage msg) throws MALTransmitErrorException
    {
        logger.log(Level.FINE, "UPCN sending msg. Target URI: {0}", new Object[]{msg.getHeader().getURITo()});

        GENOutgoingMessageHolder<byte[]> encoded =
            ((UPCNTransport)this.transport).internalEncodeMessage(
                null,
                msg.getHeader().getURITo().getValue(),
                multiSendHandle,
                lastForHandle,
                msg.getHeader().getURITo().getValue(),
                msg
            );

        logger.log(Level.FINEST, "sending message");
        try {
            this.socket.send(
                encoded.getDestinationURI(),
                encoded.getEncodedMessage()
            ).get();
            logger.log(Level.FINEST, "message sent ok");
        } catch (Throwable e) {
            logger.log(Level.FINEST,
                       "message send threw exception, rethrowing");
            throw new MALTransmitErrorException(
                msg.getHeader(),
                new MALStandardError(MALHelper.DELIVERY_FAILED_ERROR_NUMBER, null),
                null
            );
        }
    }

    public void handleTransportFailure(Throwable e) {

    }

    public void handlePayload(final String sourceURI,
                              final String destURI,
                              final byte[] payload) {
        logger.log(Level.FINEST, "handlePayload({0}, {1}, ...)",
                   new Object[]{sourceURI, destURI});
        GENMessage msg = null;
        try {
            msg = ((UPCNTransport)this.transport).createMessage(payload);
        } catch (MALException e) {
            logger.log(Level.WARNING, "failed to decode payload: {0}", new Object[]{e});
            return;
        }
        try {
            receiveMessage(msg);
        } catch (MALException e) {
            logger.log(Level.WARNING, "failed to process message: {0}", new Object[]{e});
            return;
        }
    }
}
