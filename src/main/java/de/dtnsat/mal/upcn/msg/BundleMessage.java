/**********************************************************************
LICENSE

File name: BundleMessage.java
This file is part of: mal-transport-upcn

Copyright (c) 2019 Jonas Schäfer, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************/
package de.dtnsat.mal.upcn.msg;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

public abstract class BundleMessage implements ApplicationMessage {
    private final String peerEID;
    private final byte[] payload;

    protected BundleMessage(final String peerEID,
                            final byte[] payload) {
        this.peerEID = peerEID;
        this.payload = payload;
    }

    protected BundleMessage(InputStream is) throws IOException {
        final String peer = MessageUtil.decodeEID(MessageUtil.readEID(is));
        final byte[] payload = MessageUtil.readPayload(is);
        this.peerEID = peer;
        this.payload = payload;
    }

    public String getPeer() {
        return peerEID;
    }

    public byte[] getPayload() {
        return payload;
    }

    public void serialise(OutputStream os) throws IOException {
        os.write(getVersionType());
        MessageUtil.writeEID(os, MessageUtil.encodeEID(peerEID));
        MessageUtil.writePayload(os, payload);
    }
}
