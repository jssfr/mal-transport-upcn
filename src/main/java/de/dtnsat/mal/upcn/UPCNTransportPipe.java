/**********************************************************************
LICENSE

File name: UPCNTransportPipe.java
This file is part of: mal-transport-upcn

Copyright (c) 2019 Jonas Schäfer, All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

1. Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
contributors may be used to endorse or promote products derived from
this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**********************************************************************/
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.dtnsat.mal.upcn;

import static esa.mo.mal.transport.gen.GENTransport.LOGGER;
import java.io.EOFException;
import java.io.IOException;
import static java.lang.Thread.interrupted;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ccsds.moims.mo.testbed.util.spp.SPPSocket;
import org.ccsds.moims.mo.testbed.util.spp.SpacePacket;

/**
 * Transport which bridges uPCN to the backend SPP transport.
 * 
 * This is used internally exclusively, which is why many interfaces are not
 * implemented.
 */
public class UPCNTransportPipe extends Thread {
  
  private final SPPSocket source;
  private final SPPSocket sink;
    
  public UPCNTransportPipe(SPPSocket source, SPPSocket sink)  
  {
    this.source = source;
    this.sink = sink;
  }
  
  private void handleError()
  {
    // TODO: do something sensible
  }

  @Override
  public void run()
  {
    LOGGER.log(Level.INFO, "starting UPCN transport pipe");
    // handles message reads from this client
    while (!interrupted())
    {
      try
      {
        SpacePacket pkt = source.receive();
	LOGGER.log(Level.INFO, "Received packet");
        sink.send(pkt);
	LOGGER.log(Level.INFO, "Forwarded packet");
      }
      catch (InterruptedException | EOFException ex)
      {
        LOGGER.log(Level.INFO, "Client closing connection");
        handleError();
        break;
      }
      catch (Exception e)
      {
        LOGGER.log(Level.WARNING, "Cannot read message from client", e);
        handleError();
        break;
      } 
    }
  }
    
}
